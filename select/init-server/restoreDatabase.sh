echo "sleeping to allow db to start up"
sleep 10
echo 'restoring database SelectPractice'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P $SA_PASSWORD -Q "RESTORE DATABASE [SelectPractice] FROM DISK = N'/var/opt/mssql/data/SelectPractice.bak' WITH REPLACE"
