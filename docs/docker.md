# Using Docker
We use Docker to run up instances of SQL Server and populate them with the databases, objects and data we will need for the examples. 

 * docker/docker_template:   Start a single server with a single database called *ScratchDb*
 * docker/permed-server:     Start a server configured with a non-sa login, user, roll and permissioning
 * select-examples:          Starts a server with a database called Select and a table called TelNumber

All examples have a **sa** login with password **DatabasePassw0rd**. Every example is setup by running 

```
docker-compose up --build
```

From the root folder that contains the *docker-compose.yml* file. We can then login using *Microsoft SQL Server Management Studio*. The password is *DatabasePassw0rd*. The serve name is *localhost* and we dont need a port as we used docker to expose the instance on 1433 which is the default. 

<br>

 ![Management Studio Connect](/docs/imgs/Connect.PNG)

## Docker Template
The simplest SQL server example. Start a server and adds a database called *ScratchDb*

## Permed Server
Adds a login called kennyw and a restricts access to the table to a role called kennyrole

| Key           | Description                 |
| -----------   | ----------------------------|
| Db            | ScratchDb                   |
| sa Password   | DatabasePassw0rd            |
| non -sa user  | kennyw                      |
| non -sa pword | DatabasePassw0rd            |
| Role          | kennyrole                   |

## Select Examples
Start a server and DB that we can use for the select examples from the select examples page