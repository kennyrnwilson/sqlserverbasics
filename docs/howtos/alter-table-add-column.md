# Alter Table by Adding Column with default value
## Setup

**Create table**
```sql
DROP TABLE IF EXISTS PRODUCT_TYPE;
GO

CREATE TABLE PRODUCT_TYPE
(
	Id INT IDENTITY(1,1),
	[DESCRIPTION] VARCHAR(32)
);
GO
```
**Insert values**
```sql
INSERT INTO PRODUCT_TYPE ( [DESCRIPTION] )
VALUES ( 'One' ), ( 'Two' )
```

## Alter Table to add column
We alter the table and provide a default value for the new column of -1
```sql
ALTER TABLE PRODUCT_TYPE
ADD NEW_COL INT NOT NULL DEFAULT -1
```