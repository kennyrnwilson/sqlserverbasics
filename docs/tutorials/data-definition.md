# Data Definition Tutorial

First run up the ScratchDB database in docker from the docker\scratch-db folder. See the [README](../../docker/scratch-db/README.MD)

## Create a basic table
### Question
Create a table called PRODUCT with two columns called PRODUCT_ID which is an int and DESCRIPTION which is a VARCHAR(256). The pRODUCT_ID cannot be null but the description can
### Answer

```sql
CREATE TABLE PRODUCT
(
	PRODUCT_ID INT NOT NULL,
	[DESCRIPTION] VARCHAR(256) NULL
);
```

## Drop table
### Question
Modify your answer from the previous section so that it deletes the table if it exists before creating it

### Answer
```sql

DROP TABLE IF EXISTS PRODUCT;
CREATE TABLE PRODUCT
(
    PRODUCT_ID INT NOT NULL,
    [DESCRIPTION] VARCHAR(256) NULL
);
```

## Primary Key Constraint
### Question
What is the effect of a primary key constraint
### Answer
A table can have only one primary key constraint and all columns which form part of the primary key must be non-null. 
### Question
What happens when one creates a primary key on a table?
### Answer
SQL server creates a unique clustered index on the table.
### Question
What is the exception to this rule?
### Answer
If we explicitly specify that the primary key should use a non-clustered index.
### Question
Create a table EMPLOYEE with two varchar columns FIRSTNAME and SURNAME which form part of a composite primary key constraint

### Answer
```sql
DROP TABLE IF EXISTS EMPLOYEE;
CREATE TABLE EMPLOYEE
(
	FIRSTNAME VARCHAR(32) NOT NULL,
	SURNAME VARCHAR(32) NOT NULL
	CONSTRAINT PK_EMPLOYEE_FIRSTNAME_SURNAME PRIMARY KEY CLUSTERED(FIRSTNAME, SURNAME)
);
```

## Unique Constraints

