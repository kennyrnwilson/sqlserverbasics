# Tables and Indices

## Heaps and B-trees
In SQLServer a table is structured as either a **heap** or a a **B-tree**. For this reason a table is often known as HOBT. A table is structured as a B-tree when it has a **clustered index** on it, otherwise it is structured as a heap. Most tables tend to use the B-tree structure. **Non-clustered indexes** are themselves structured as B-trees. 

We should avoid using a heap in the following situations

### Avoid Heap When
 * If data is frequently accessed in sorted order
 * If data is frequently grouped
 * If range queries are often used

The following query on this table 

```sql
select * from TelNumber
order by PersonId
```

Has a query plan of

**Query Plan**

![PLan](/docs/imgs/indices/heap_sort_plan.PNG)

