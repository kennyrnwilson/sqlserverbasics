# Hekaton
Most relational databases were designed in the days when disks were cheap and memory was expensive. Microsoft's In-memory OLTP offering was designed to take advantage of the fact that with modern hardware a reasonable amount of data can be kept in memory at all times. Hekaton keeps the entire working set, including indices in main memory at all times. There are two flavours of hekaton tables, schema only and schema backed by disk. If we only access in-memory tables we can also natively compile our stored procedures which gives another huge performance boost.




## Performance
We benchmarked performance with a prod spec machine using 20 cores and 768GB of main memory. We benchmarked the number of transactions/inserts we could achieve per second in the following setups.

 * Disk-based SQL Server : 94 transactions per ms
 * In-memory, schema and data, non-native procs : 
 * In-memmory, schema and data, native procs : 1222 transactions per ms
 * In-memory, schema only, native procs : 1992 transaction per ms

 By far the biggest improvement came when we switched to schema only and native procs. Under these consitions we went from 94 transaction per millisecond to 1992 transactions per millisecond.