# READ UNCOMMITTED
This is the lowest isolation level. In this level a transaction does not request an exclusive lock on resources. As such any transaction in this level is never in conflict with a writer holding an exclusive lock so I can see **dirty** reads. Run the following code in a scratch database

**Setup Table**

```sql
DROP TABLE IF EXISTS ACCOUNT;

CREATE TABLE ACCOUNT
(
    SURNAME varchar(25) NOT NULL,
    BALANCE NUMERIC
);

INSERT INTO ACCOUNT (SURNAME, BALANCE)
VALUES ('WILSON', 100), ('SMITH', 200)
```
**Partial Update**
Now run the following in one session putting a breakpoint after the first update (line 6). 
```sql
BEGIN TRAN
	UPDATE ACCOUNT 
	SET BALANCE = 90
	WHERE SURNAME = 'WILSON'

	UPDATE ACCOUNT 
	SET BALANCE = 210
	WHERE SURNAME = 'SMITH'

COMMIT TRAN
```
**Dirty Read**
Now open a second session while the first session is on a breakpoint and run the following
```sql
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT * FROM ACCOUNT
```

The situation we have is depicted as follows

![READ UNCOMMITTED](/docs/imgs/transactions/isolation_read_uncommitted.png)

