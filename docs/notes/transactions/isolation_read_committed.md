# READ COMMITED
At this level a reader must acquire a shared lock on the resource. When the writer has an exclusive lock this causes the reader to block. This is the default isolation level for SQL Server in the box disk-based tables. The exclusive lock is held only for t

One thing to note about this isolation level is that the reading transaction only holds the shared lock until it is done with the resource. In practice this can lead to a situation which is refered to as **nonrepeatable reads**. We first show the scenario that read commited fixes and then show a non-repeatable read

## Prevents dirty reads 

**Setup Table**

```sql
DROP TABLE IF EXISTS ACCOUNT;

CREATE TABLE ACCOUNT
(
    SURNAME varchar(25) NOT NULL,
    BALANCE NUMERIC
);

INSERT INTO ACCOUNT (SURNAME, BALANCE)
VALUES ('WILSON', 100), ('SMITH', 200)
```
**Partial Update**
Now run the following in one session putting a breakpoint after the first update (line 6). 
```sql
BEGIN TRAN
	UPDATE ACCOUNT 
	SET BALANCE = 90
	WHERE SURNAME = 'WILSON'

	UPDATE ACCOUNT 
	SET BALANCE = 210
	WHERE SURNAME = 'SMITH'

COMMIT TRAN
```
**Dirty Read**
Now open a second session while the first session is on a breakpoint and run the following
```sql
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
SELECT * FROM ACCOUNT
```

Now the reader blocks while the update is on the breakpoint. 

![READ COMMITTED](/docs/imgs/transactions/isolation_read_committed.png)

## Does not prevent nonrepeatable reads. 
**Setup Table**

```sql
DROP TABLE IF EXISTS ACCOUNT;

CREATE TABLE ACCOUNT
(
    SURNAME varchar(25) NOT NULL,
    BALANCE NUMERIC
);

INSERT INTO ACCOUNT (SURNAME, BALANCE)
VALUES ('WILSON', 100), ('SMITH', 200)
```

For this scenario. We run the following code first and put a break point after the first read (line 6)

```sql
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
BEGIN TRAN
	
	SELECT * FROM ACCOUNT

	SELECT * FROM ACCOUNT

COMMIT TRANSACTION
```

Now we run the update transaction completely and then let our read transaction complete. We see this.

![NONREPEATABLE READ](/docs/imgs/transactions/isolation_read_committed_nonrepeatable_read.png)

Each read saw a different value