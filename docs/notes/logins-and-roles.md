# Logins and Roles
A login is a server level entity and users are database level entities. We can have a login with no user associated with it. In this case we can log onto the server but not use any of the databases on the server. 

## Pre-requisite, Create a table
In order to show permissioning we need a table object to work with. Run the following as sa

```sql
CREATE TABLE dbo.Products
(
	prodId INT NOT NULL,
	description VARCHAR(30) NOT NULL
);
```


## Creating a Login
If we are logged onto the server as *sa* we can add a user using the following command.

``` sql
CREATE LOGIN kennyw  WITH PASSWORD = 'DatabasePassw0rd';
```

We can achive the same thing in *SQL Server Management Studio* by right-clicking on the folder *<ServerName>/Security/Logins* and select New Login.

 ![Create Login](../imgs/create-login.png)

 If we want to view the existing logins we cn run the command 

 ``` sql
 SELECT * FROM syslogins 
 ```

 or we can just open the folder *<ServerName>/Security/Logins* in *SQL Server Management Studio*

## Creating a User
As we mentioned, a user is a database level concern. We can create a new user for a database and associated it with a server login by executing the following command 

``` sql
CREATE USER kw FOR LOGIN kennyw;
```
We can achieve the same thing using *SQL Server Management Studio* by right-clicking on *Databases/<DatabaseName>/Security/Users* and choosing *New User*. On the setup dialogue enter the name and the login we want to associate the user with. You should now be able to log on to the server with the login and then access the database. 

## Creating a Role
To be able to do anything we need to create a role that can read our table. We do so this as follow which creates a role oweded by the user that executes CREATE_ROLE command

```sql
CREATE ROLE kennyrole  
```

 We can do this in *SQL Server Management Studio* as follows. On the folder Databases/*<Database>/Security/Roles* select New Database Role. 

 ![New Role](../imgs/new-role.png)

 ### Add permissions to a role
We can add select permission on the table Products to the role as follows.
 ```sql
 GRANT SELECT ON Products To kennyrole
 ```

We can do this in *SQL Server Management Studio* as follows. Right click on role and select properties and then on the dialogue select Securables. 

 ![Securables](../imgs/role-securables.png)

## Associate Role with User
We can associate a user with our role as follows

```sql
ALTER ROLE kennyrole ADD MEMBER kw
```
We can do this in *SQL Server Management Studio* as follows. Right click on user and select properties then on the Membership page add the new role we created.

 
 ![Securables](../imgs/role-to-user.png)

 We can now read the table.

 ## Docker Setup
 
 The folder docker/permed-server uses docker to run up a SQL server with a login, a user and some permissioned objects. 
