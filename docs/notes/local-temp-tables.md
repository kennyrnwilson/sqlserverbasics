# Local Temporary Tables

Local temporary tables are created in *tempdb*. 

 These are only visible within creating session. In addition they are scoped with the call stack. They are only visible at the nesting level in which they are created and to inner nesting levels of the call stack. If the nesting level in which they are created is left SQL server automatically destroys the temporary table. 

If a temporary table is created in the outermost nesting level of a session it is available to all batches in that session and SQL server destrous it automatically when the connecting session disconnects.  

Furthermore they are only visible within the creating scope and any inner scopes withing the call stack. With the scope is exited the temporay table is automatically destroyed by SQL Server. 

**Using Temporary Tables**

```sql
DROP TABLE IF EXISTS #MyPerson;
GO

CREATE TABLE #MyPerson
(
	FIRSTNAME VARCHAR(32)
);

INSERT INTO #MyPerson
(
	FIRSTNAME
)
VALUES
( 'Kenny');

SELECT * FROM #MyPerson;

DROP TABLE IF EXISTS #MyPerson;
```

## Questions
**Why use temporary tables?**

* We want to store intermediate results, perhaps as part of a cursor. 
* We need to access the same results multiple times.


