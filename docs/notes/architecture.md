# SQL Server Architecture
Examples of the concepts discussed in this section can be found in [Docker](/docker/architecture/)

**Terms**


| Term                | Description                                                         | 
| -----------         | --------------------------------------------------------------------| 
| Server              | A Machine on which SQL Server instances live                        |
| Instance            | A Single SQL Server process. Can contain multiple database          |
| Database            | Each instance has many system and user databases.                   |
| Page                | Fundamental unit of storage for SQL Server disk-based tables - 8Kb  |
| Extent              | Set of 8 contiguous pages or 64KB.                                  |
| Allocation Unit     | Set of pages that contain a particular type of data for a table     |
| Partition           | Each table or index is made of one or more partitions               |


Partition - Each HOBT or non-clustered index is made of one or more paritions. The parititions can be spread across database **file groups**. 


## Servers and Instances 
A single server machine can host multiple instances of SQL server. Each instance is completely separate from the other instances. Where we have multiple instances on a single machine, one can be designated the default instance, whereas all others must be named instances. Clients can connect to a default instance by simply specifying the hostname or IP address. To connect to a named instance the client must specify the hostname and instance name separated by a backslash.


## Databases
Each instance contains multiple databases. An initial install will create a set of system databases 

 * master - instance wide metadata about databases in the instance
 * Resource - holds definitions of all system objects. Backs the sys schema
 * model - template for all new databases created
 * tempdb - hold temp tables, created and destroyed on a reboot
 * msdb

 Users then add one or more user databases.

<br>

![Server and Databases](/docs/imgs/architecture/server-and-databases.png)

## Disk Files
At the disk level there are a number of files we need to be aware of. In SQL Server all data files are organised into logical **file groups.**  When we create a table or index we target a file group. The data for the object is then spread across the files in the file group.  

 Every database needs at least one log file and one data file. Data files hold object data and the log files hold information needed to perform transactions. 

  * Log File - Used in transactions. SQL server can only write one log file at a time sequentially. 
  * Primary data file (.mdf) - holds the databases system catalogue
  * Secondary data files (.ndf) - optionally we can add secondary data file in user filegroups to hold data

<br>

  ![Pysical Layer](/docs/imgs/architecture/pysical-layer.png)

## Pages
The page is the fundamental unit of storage in SQL Server. The disk space of a data file such as *MyDatabase_dat2.ndf* is logically numbered from 0 to n. Each page is 8KB in size 


  ![Pages](/docs/imgs/architecture/pages.png)

  Pages on disk can contain different types of data. The page header is 96 bytes and includes the page number, page type, ID of the allocation unit of the object that owns the page. The following list the types of data a page can hold

**Page Types**

  * Data 
  * Index
  * Global Allocation Map
  * Shared Allocation Map
  * Page Free Space
  * Index Allocation Map

In a data page rows are inserted serially into the page after the header. At the bottom of the page is a row offset table that enables one to index into the rows quickly. 

  ![Data Page](/docs/imgs/architecture/data_page.png)

## Extents
An extent is a set of 8 contiguous pages or 64KB. With a uniform extent all pages are owned by the same object. Mixed extents on the other hand can be shared by up to eight different object. 

## Allocation Units
Each partition of each table can contain three kinds of data. Each type of data is stored its own allocation unit

 * IN_ROW_DATA
 * ROW_OVERFLOW_DATA
 * LOB_DATA

An allocation unit is then just the set of pages that contain a particular type of data for a single partition of a table. All table paritions will have at least an IN_ROW_DATA allocation unit. Whether there are ROW_OVERFLOW_DATA and LOB_DATA depends on the structure of the table and the data rows in it. We can view the allocation units for a table using the command

```SQL
SELECT type_desc, total_pages, used_pages,data_pages 
FROM sys.allocation_units
WHERE container_id = (SELECT partition_id FROM sys.partitions 
WHERE OBJECT_ID = OBJECT_ID('Person'))
```

If we want even more information we can use the *dm_db_database_page_allocations* table valued function

```sql
SELECT * 
FROM sys.dm_db_database_page_allocations(DB_ID(), OBJECT_ID('dbo.Person'),NULL,NULL,'LIMITED')
```

In this case we just have one allocation unit but we could have three as the diagram shows. 

  ![Allocation Units](/docs/imgs/architecture/allocation-units.png)

  An **Index Allocation Map** or **IAM** is a page that holds information about the extents used by a tables allocation unit. The IAM page covers a 4GB range in a file. The following diagram show the folllowing features of an IAM. 

   1. It is on a page that is randomly inserted in the file
   2. It is for a single allocation type of a single object
   3. In this case the table that owns the allocation unit uses two extents. 

  ![IAM](/docs/imgs/architecture/iam.png)


