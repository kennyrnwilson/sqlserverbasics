# Table Variables and Types
Table variables are created in *tempdb*. They are visible only to the creating session. They are even more limited than temporary tables as they are only visible in the current batch. 


**Using Temporary Tables**

```sql
DECLARE @MyTable TABLE
(
	FIRSTNAME VARCHAR(32)
);

INSERT INTO @MyTable
(
	FIRSTNAME
)
VALUES
( 'Kenny');

SELECT * FROM @MyTable;
```

## Table Types
We can create table variables which we can use with table variables and as the parameters of stored procedures and user defined functions.

```sql
--Create Table Type
DROP TYPE IF EXISTS MYTYPE;
CREATE TYPE MYTYPE AS TABLE
(
	FIRSTNAME VARCHAR(32)
);
GO

--Use Table Type 
DECLARE @MYVAR AS MYTYPE;
INSERT INTO @MYVAR
(
	FIRSTNAME
)
VALUES
( 'Dave');

SELECT * FROM @MYVAR;

```

## Questions
**When use temporary tables versus tables variables?**

In terms of performance use table variables for small amounts of data and temporary tables for larger amounts. 

