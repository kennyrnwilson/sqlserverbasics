# Select
## Phases
 * [5]  SELECT empid, YEAR(orderdate) AS orderyear, COUNT(*) AS numorders
 * [1]  FROM Sales.Orders
 * [2]  WHERE custid = 71
 * [3]  GROUP BY empid, YEAR(orderdate)
 * [4]  HAVING COUNT(*) > 1
 * [6]  ORDER BY empid, orderyear;

| Number           | Clause                 | Descriptions |
| -----------   | ----------------------------|----------------------------|
|1| **FROM**|Specify the table we want to query|
|2| **WHERE**| Uses a predicate to filter the rows returned by the FROM phrase. Where clauses enable the use of indices to improve performance and reduce the network traffic that would occur if we performed a table scan and filtered on the client.  |
|3| **GROUP BY**| Produce a group for each unique combination of values specified in this clause. |
|4| **HAVING** | Uses a predicate to filter the groups returned. Can utilise aggregate functions in the predicate.|
|5| **SELECT** | Specify the columns we want to see in the result |
|6| **ORDERBY**| Sort the rows for presentation purposes

### GROUP BY
If a query contains a group by phase any subsequent HAVING, SELECT, and ORDERBY clauses work on groups. As such they can only operate on expressions that return a single scalar value per group. Any fields specified in the GROUP BY phase implicitly have this process. Any elements that do not meet this restriction can only be used as inputs to aggregation functions such as COUNT, SUM, AVG, MIN, MAX.

### ORDER BY
In SQL, a table has no order. By using order by the result is ordered and hence cannot be considered a table. In SQL the ordered rows are referred to as a cursor. Unlike all other phases the order by phase can utilise column alisases defined in the select phase as the order by is the only phase that follows the select phase. Returns a cursor rather than table. 

## Phase Illustrations
To run these examples load the **docker/select-examples** folder and use the database called **Select**
### SELECT

```sql
SELECT *
FROM TelNumber
```
 ![Select](/docs/imgs/select/select_basic.png)
 
### WHERE
The Where phrase adds a predicate to filter the results. 

```sql
SELECT *
FROM TelNumber
WHERE PersonId = 1
```
 ![Where](/docs/imgs/select/select_and_where.png)

### GROUP

Allows grouping. The select can only work on columns appropriate to the grouping.

```sql
Select PersonId, COUNT(Number) AS 'Number Count'
FROM TelNumber
WHERE PersonId IN (1,2)
GROUP BY PersonId
```
 ![Select](/docs/imgs/select/select_groupby.png)

### HAVING

The having Phase filters the results of the Group By phase

```sql
Select PersonId, COUNT(Number) AS 'Number Count'
FROM TelNumber
WHERE PersonId IN (1,2)
GROUP BY PersonId
HAVING PersonId = 1
```

![Select](/docs/imgs/select/select_having.png)

### ORDER BY

Order By returns a cursor rather than a table. 

```sql
Select PersonId, COUNT(Number) AS 'Number Count'
FROM TelNumber
WHERE PersonId IN (1,2)
GROUP BY PersonId
ORDER BY PersonId DESC
```

 ![Select](/docs/imgs/select/select_orderby.png)

**Usage Notes**
  * It is the only Phase that can access aliases from the SELECT Phase
  * We can order by attributes not in the select phase if we do not use DISTINCT.
  * If we use DISTINCT, the ORDER BY elements must be in the SELECT phase.

  The reason being is that with DISTINCT there are multiple rows for each distinct value and it is not clear which one to use for the ordering. 