# Batches and Variables

## Variables
A variable can be declared and assigned to separately as follows

```sql
DECLARE @MY_INT INT
SET @MY_INT = 2
SELECT @MY_INT
```
or in one statement as follows

```sql
DECLARE @MY_INT INT = 4
SELECT @MY_INT
```
When assigning to a scalar variable the value must be the result of a scalar expression. This expression can be a literal as above or it can be the result of a scalar subquery as follows.

```sql
DECLARE @SECOND_NAME VARCHAR(32);
SET @SECOND_NAME =
(
    SELECT SecondName FROM Person2 Where FirstName = 'Sam'
);
SELECT @SECOND_NAME
```
This standard SQL syntax can only be used to set one variable at a time. T-SQL has a non-standard extention that can set multiple variables in one statement as follows. 

```sql
DECLARE @SECOND_NAME VARCHAR(32);
DECLARE @FIRST_NAME VARCHAR(32);
SELECT @FIRST_NAME = FirstName,
       @SECOND_NAME = SecondName
FROM Person2
Where FirstName = 'Sam'
SELECT @SECOND_NAME,
       @FIRST_NAME

```

## Batches
A batch is one or more T-SQL statements sent to the server at the same time. Batches should not be confused with transactions. A batch can consist of multiple transactions and single transaction can be carried out over multiple batches. Variables are scoped to the batch in which they are defined. 

Some types of statements cannot be combined with other statements in the same batch. E.g. DROP and CREATE TABLE. In such cases we can get by this by using the GO command to instruct the client application to create multiple batches. 