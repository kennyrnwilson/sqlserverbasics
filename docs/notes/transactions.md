# Transactions
Transaction are used to control concurrent access to data. Each transaction forms a unit of work that can access and modify data. Transactions have four properties: **A**tomicity, **C**onsistency, **I**solation and **D**urability that form the acroynym **A.C.I.D**

## A.C.I.D
### Atomicity
Since a transaction forms an atomic UOW then either all the changes occur or none occur. 

### Consistency

Transactions should move the database from one consistent state to another. The actual level of consistency is defined by **"Isolation Levels"**. 

### Isolation
With isolation transactions only access consistent data. What that means is controlled by **"Isolation Levels"**. Disk based tables have has two different mechanisms to handle isolation

 * Locking
 * Locking and row versioning 

 By default SQL Server using locking and readers must acquire shared locks. When there is inconsistent state readers must block.

### Durability
Changes are written to the transaction log on disk before it is written to the database disk file. 


## Locks
There are two lock modes: *exclusive* and *shared*.All modifications inside a transaction require an exclusive lock. Any transaction that modifies data requires an exclusive lock on the resources it will modify. A transaction can never obtain an exclusive lock on a resouce if another transaction holds either an exclusive or shared lock on the resource. Similarly no transaction can obtain a shared or exclusive lock on a resource if another transaction has an exlusive lock on the same resource. 

Isolation levels control how reading works. By default in SQL Server box product, disk based tables use an isolation level of **READ COMMITTED**. In this mode, if one transaction is modifying rows, no other transaction can read them. This is known as *pessimistic concurrency*. 

> Data that was modified by one transaction can neither be modified nor read (at least by default in a SQL Server box product) by another transaction until the first transaction finishes. And while data is being read by one transaction, it cannot be modified by another.

**-SQL Fundamentals, Itzik Ben Gan**

## Isolation Levels
There are four isolation levels in a pure-locking model and two levels in the locks and row versioning model.  

 * [READ UNCOMMITTED](./transactions/isolation_read_uncommitted.md)

 * [READ COMMITTED](./transactions/isolation_read_committed.md)