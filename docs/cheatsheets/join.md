# Joins
## Initialisation
The examples in this section use the tables Person and TelNumber from the ScratchDb. Load it by running 

```
docker-compose up --build
```

from the docker/docker-template folder. 

## Cross Join
A cross join is a simple cartesian product
```sql
SELECT P.id As 'PersonId',
       T.id AS 'NumberId'
FROM Person P
    CROSS JOIN TelNumber T

```

 ![Select](/docs/imgs/joins/cross-join.png)

## Self-Cross Join
```sql
SELECT P1.id AS 'Person1',
       P2.id AS 'Person2'
FROM Person P1
    CROSS JOIN Person P2

```

 ![Self Cross Join](/docs/imgs/joins/self-cross-join.png)

 ## Inner Join
 An inner join is implemented in two logical phases; a cartesian product followed by a filtration based on a predicate specified in the ON clause. 

 ```sql
SELECT P.FirstName,
       P.SecondName,
       T.Number
FROM Person P
    INNER JOIN TelNumber T
        ON P.Id = T.PersonId
 ```
 Note that the third row in the Person table is excluded from the result because it does not have any matching rows in the TelNumber table. Similarly, the fourth row in the TelNumber table is excluded from the result as it has no match in the Person table. This the defining feature of an inner join.  

 ![inner Join](/docs/imgs/joins/inner-join.png)

  ## Composite Join
  A composite join is just a join where the AS clause has multiple attributes e.g., something like

  ```sql
  SELECT P.firstName AS 'First Name',
       P.secondName AS 'Second Name',
       T.telNumber AS 'Num'
FROM Person AS P
    INNER JOIN TelNumber AS T
        ON P.secondName = T.secondName
           AND P.firstName = T.firstName

```

 ## Non-Equi Join 
 The join condition has any operator other than equality 

 ```sql
 SELECT P1.firstName + ' ' + P1.secondName AS 'Person1',
       P2.firstName + ' ' + P2.secondName AS 'Person2'
FROM Person AS P1
    INNER JOIN Person AS P2
        ON P1.id < P2.id

 ```

  ![Non-Equi Join](/docs/imgs/joins/non-equi-join.png)

 ## Multi-Join
 Joining more than one table. Logically multi joins proceed from left to right with the result of the first table operator becoming the left input to the second table operator and so on. 
```sql
SELECT P.FirstName,
       P.SecondName,
       T.Number,
       NT.[Type]
FROM Person P
    INNER JOIN TelNumber T
        ON P.id = T.personId
    INNER JOIN NumberType NT
        ON T.id = NT.NumId
```

 ![Multi-Join](/docs/imgs/joins/multi-join.png)

## Outer Join
  An inner join leaves out any rows that do not match as follows

  ```sql
SELECT P.FirstName, P.SecondName, T.Number
FROM Person P
	INNER JOIN TelNumber T
		ON P.Id = T.PersonId
 ```
 ![inner Join](/docs/imgs/joins/inner-join.png)

Outer joins enable us to fix this problem.


### Left Outer Join
 If we want to add back in row 3 in the Person table, we can use a LEFT OUTER JOIN

 ```sql
SELECT P.FirstName,
       P.SecondName,
       T.Number
FROM Person P
    LEFT OUTER JOIN TelNumber T
        ON P.Id = T.PersonId
 ```

 ![Left Outer Join](/docs/imgs/joins/left-outer-join.png)

 ### Right Outer Join
 If we want to add back in row 4 in the TelNumber table, we can use a RIGHT OUTER JOIN. 

 ```sql
SELECT P.firstName,
       P.secondName,
       T.number AS 'Number'
FROM Person AS P
    RIGHT OUTER JOIN TelNumber AS T
        On P.id = T.personId

 ```

 ![Left Outer Join](/docs/imgs/joins/right-outer-join.png)

  ### Full Outer Join
  If we want to add back in row 4 in the TelNumber table row and row 3 in the Person table, we can use a FULL OUTER JOIN

  ```sql
SELECT P.FirstName,
       P.SecondName,
       T.Number
FROM Person P
    FULL OUTER JOIN TelNumber T
        ON P.Id = T.PersonId

  ```

   ![Full Outer Join](/docs/imgs/joins/full-outer-join.png)

### Outer Join versus Where
The ON clause determines which rows match between the two tables. The WHERE clause filters the matched rows. 

### Outer Join versus Where
The following shows how to only return outer rows.

```sql
SELECT P.FirstName, P.SecondName, T.Number
FROM Person P
LEFT OUTER JOIN TelNumber T
	ON P.Id = T.PersonId
WHERE T.number IS NULL

```

   ![Outer rows only](/docs/imgs/joins/outer-rows-only.png)

### Outer Join Bugs
In general, we should never refer to columns in the non-preserved side of an outer join from the WHERE clause. This will remove any rows without a match in the non-preserved side effectively changing the join to an INNER JOIN. 
Similarly, if we have a multiple table join where we first perform an outer join between two tables and then perform an inner join between a third table and a field on the non-preserved side of the outer join then any outer rows will be discarded. 
