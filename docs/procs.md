# Procedures
## Create a Proc
The following shows how to create a simple PROC

```sql
CREATE PROC GetPeople
(
    @SECOND_NAME VARCHAR(256)
)
AS 
  SELECT * From Person
  WHERE SecondName = @SECOND_NAME
GO
```

and then how we call it
```sql
EXEC GetPeople 'Jones'
```