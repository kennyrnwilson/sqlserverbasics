# SQL Server

This repository contains information pertaining to working with SQLServer in an enterprise environment. The information is split into section that cover.

* Notes 
* Howtos
* Cheat sheets
* Tutorials

Let us cover each in turn

## Notes

 * [Architecture](./docs/notes/architecture.md)
 * [Logins, Users and Roles](./docs/notes/logins-and-roles.md)
 * [Tables and Indices](./docs/notes/tables-and-indices.md)
 * [Select](./docs/notes/select-clause.md)
 * [Batches and Variables](./docs/notes/batches-and-variables.md)
 * [Transactions](./docs/notes/transactions.md)
 * [Local Temporary Tables](./docs/notes/local-temp-tables.md)
 * [Table Variables and Types](./docs/notes/table-variables-and-types.md)
 * [Stored Procedures](./docs/notes/stored-procedures.md)
 * [In-memory OLTP / Hekaton](./docs/notes/hekaton/intro.md)
## Cheat Sheets
 * [Data Definition](./docs/cheatsheets/data-definition.md)
 * [Data Modification](./docs/cheatsheets/data-modification.md)

## How-To

  * [Sproc Out Prams](./docs/howtos/stored-proc-out-params.md)
  * [Alter Table Add Column](./docs/howtos/alter-table-add-column.md)

## Tutorials

  * [Data Defintion](./docs/tutorials/data-definition.md)

I use Docker to run up SQL Server instances and populate them with the objects and data I need to run the examples. See the [Use of Docker](./docs/docker.md) section for instructions on how to run the samples. 
