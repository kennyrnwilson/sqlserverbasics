IF DB_ID('MyDatabase') IS NOT NULL
   print 'db exists'
ELSE 
    create database MyDatabase
GO

USE master;
GO
ALTER DATABASE MyDatabase
ADD FILEGROUP MyDatabaseFileGroup 
GO

ALTER DATABASE MyDatabase ADD FILE
(NAME="MyDatabase_dat2", FILENAME='/var/opt/mssql/data/MyDatabase_dat2.ndf')
TO FILEGROUP MyDatabaseFileGroup;
GO

ALTER DATABASE MyDatabase
ADD FILEGROUP MyInMemoryOLTP_mod CONTAINS MEMORY_OPTIMIZED_DATA;
GO/var/opt/mssql/data/MyInMemoryOLTP_mod

ALTER DATABASE MyDatabase ADD FILE
(NAME=MyInMemoryOLTP_mod, FILENAME='/var/opt/mssql/data/MyInMemoryOLTP_mod')
TO FILEGROUP MyInMemoryOLTP_mod;


USE MyDatabase
GO

CREATE TABLE Person
(
	FirstName VARCHAR(20) NOT NULL,
	SecondName VARCHAR(20) NOT NULL,
) ON MyDatabaseFileGroup
GO


