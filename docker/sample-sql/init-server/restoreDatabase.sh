echo "sleeping to allow db to start up"
sleep 30

echo 'restoring database Select'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'DatabasePassw0rd' -Q "RESTORE DATABASE [Select] FROM DISK = N'/var/opt/mssql/data/Select.bak' WITH REPLACE"

# echo "sleeping to allow select to start"
# sleep 30

echo 'Creating Db on Server'
/opt/mssql-tools/bin/sqlcmd  -S sql-server-instance -U 'SA' -P  'DatabasePassw0rd' -i ./scripts/create-scratchdb-db.sql