echo "sleeping to allow db to start up"
sleep 30
echo 'restoring database TSQLV3'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'DatabasePassw0rd!' -Q "RESTORE DATABASE [TSQLV3] FROM DISK = N'/var/opt/mssql/data/TSQLV3.bak' WITH REPLACE"

echo 'restoring database PerformanceV3'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'DatabasePassw0rd!' -Q "RESTORE DATABASE [PerformanceV3] FROM DISK = N'/var/opt/mssql/data/PerformanceV3.bak' WITH REPLACE"

echo 'restoring database TSQLV4'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'DatabasePassw0rd!' -Q "RESTORE DATABASE [TSQLV4] FROM DISK = N'/var/opt/mssql/data/TSQLV4.bak' WITH REPLACE"

echo 'restoring database Select'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'DatabasePassw0rd!' -Q "RESTORE DATABASE [Select] FROM DISK = N'/var/opt/mssql/data/Select.bak' WITH REPLACE"

echo 'restoring database Joins'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'DatabasePassw0rd!' -Q "RESTORE DATABASE [Joins] FROM DISK = N'/var/opt/mssql/data/Joins.bak' WITH REPLACE"

echo 'restoring database AdventureWorks'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'DatabasePassw0rd!' -Q "RESTORE DATABASE [AdventureWorks] FROM DISK = N'/var/opt/mssql/data/AdventureWorks2019.bak' WITH MOVE  'AdventureWorks2017' to N'/var/opt/mssql/data/AdventureWorks2019.mdf', MOVE 'AdventureWorks2017_Log' to N'/var/opt/mssql/data/AdventureWorks2019_log.ldf'"

echo 'restoring database WideWorldImporters-Full.bak'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'DatabasePassw0rd!' -Q "RESTORE DATABASE [WideWorldImporters] FROM DISK = N'/var/opt/mssql/data/WideWorldImporters-Full.bak' WITH REPLACE"
 
echo 'restoring database WideWorldImporters'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'DatabasePassw0rd!' -Q "RESTORE DATABASE WideWorldImporters FROM DISK = '/var/opt/mssql/data/wwi.bak' WITH MOVE 'WWI_Primary' TO '/var/opt/mssql/data/WideWorldImporters.mdf', MOVE 'WWI_UserData' TO '/var/opt/mssql/data/WideWorldImporters_userdata.ndf', MOVE 'WWI_Log' TO '/var/opt/mssql/data/WideWorldImporters.ldf', MOVE 'WWI_InMemory_Data_1' TO '/var/opt/mssql/data/WideWorldImporters_InMemory_Data_1'"
