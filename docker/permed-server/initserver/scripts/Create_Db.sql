IF DB_ID('ScratchDb') IS NOT NULL
   print 'db exists'
ELSE 
    create database ScratchDb
GO

-- Create a table object that we can permission
USE ScratchDb
CREATE TABLE Products
(
	prodId INT NOT NULL,
	description VARCHAR(30) NOT NULL
);
GO

-- Add a custom login to the SQL server instance
CREATE LOGIN kennyw WITH PASSWORD = 'DatabasePassw0rd';
GO

-- Add a user and associate it with the login we created in the previous step
CREATE USER kw FOR LOGIN kennyw;
GO

-- Create a role
CREATE ROLE kennyrole
GO

-- Give the role select on our table
GRANT SELECT ON Products To kennyrole

-- Add the role to our user
ALTER ROLE kennyrole ADD MEMBER kw