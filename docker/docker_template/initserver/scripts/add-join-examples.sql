USE ScratchDb;

DROP TABLE IF EXISTS Person;
GO

CREATE TABLE Person
(
	Id INT IDENTITY(1,1),
	FirstName VARCHAR(32),
	SecondName VARCHAR(32),
	CONSTRAINT PK_Person PRIMARY KEY CLUSTERED (Id)
);
GO

INSERT INTO Person
(
	FirstName, 
	SecondName
)
VALUES
( 'Kenneth', 'Wilson'),
( 'John', 'Smith');
GO

DROP TABLE IF EXISTS TelNumber;
GO

CREATE TABLE TelNumber
(
	Id INT IDENTITY(1,1),
	PersonId INT,
	Number VARCHAR(32),
);
GO

INSERT INTO TelNumber
(
	PersonId,
	Number
)
VALUES
( 1, '02084357777'),
( 1, '07999321123'),
( 2, '01324256123');
GO


DROP TABLE IF EXISTS NumberType;
GO

CREATE TABLE NumberType
(
	NumId INT,
	[Type] VARCHAR(32)
);
GO

INSERT INTO NumberType
(
	NumId,
	[Type]
)
VALUES
(1, 'Home'),
(2, 'Mobile'),
(1, 'Home'),
